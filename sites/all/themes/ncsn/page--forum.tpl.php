<?php include('header.tpl.php');?>

<style type="text/css">
/* Removes Add New Forum Topic Link */

ul.action-links {list-style:none; list-style-type:none; margin-left:-20px;}
ul.action-links li a{font-size:14px; text-decoration:none;}
ul.action-links li a:hover {text-decoration:underline;}
</style>



<div id="content" style="margin:20px 0 0 0;">
<div class="container">

		<div id="main" style="width:100%;">
	<a id="main-content"></a>
      <div class="entry single">

				<div class="entry-header">

					 <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h2 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

					
					
					
					</div><!-- end .entry-header -->

		<?php print render($page['highlighted']); ?>
      <div class="breadcrumb"><a href="/">Home</a> &nbsp; > &nbsp; <a href="#"><?php print $title; ?></a> </div>
	 <?php print $messages; ?>
      <?php if ($tabs = render($tabs)): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>

    
    			
					<div class="main-internal-sidebar">
					
					
					<?php print render ($page['sidebar_first']);?>

					</div>
					
					
<div class="entry-content" style="width:750px; float:left;">
				
<?php
global $user;
  
  if ($user->uid == 0) { 
	echo "<center><p style='font-size:16px;'>If you wish to view, contribute and exchange ideas on 'The Network' you have be a member of NCSN. 
 Call 01244-322314 to discuss membership options.<br/><br/>
If you are already a member, please <a href='/user/login?destination=forum'>Login</a>.</p></center>";
  } 
  else {    
  					  print render($page['content']);
}
?>
      <?php print $feed_icons; ?>

				</div><!-- end .entry-content -->
				
				<div class="clear"></div>

				<div class="entry-footer">

					
				<?php include('contact_info.tpl.php')?>
				</div><!-- end .entry-footer -->
				
			</div><!-- end .entry -->

		</div><!-- end #main -->

		
		<div class="clear"></div>

	</div><!-- end .container -->

</div><!-- end #content -->

<?php include('footer.tpl.php');?>

</body>
</html>
