

<?php include('header.tpl.php');?>

<div id="content" style="margin:20px 0 0 0;">
	<div class="container">

		<div id="main">
	<a id="main-content"></a>
      <div class="entry single">

				<div class="entry-header">

					 <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h2 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

					
					
					
					</div><!-- end .entry-header -->

		<?php print render($page['highlighted']); ?>
 <div class="breadcrumb"><a href="/">Home</a> &nbsp; > &nbsp; <a href="/information">Information</a> &nbsp; > &nbsp;  <a href="#"><?php print $title; ?></a> </div>



    			
					
				
				<div class="entry-content">
				
					       <?php print $messages; ?>
      <?php if ($tabs = render($tabs)): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
<?php
global $user;
 if ($user->uid == 0) { 
	echo "<center><p style='font-size:16px;'>If you wish to view, contribute and exchange ideas on 'The Network' you have be a member of NCSN. 
 Call 01244-322314 to discuss membership options.<br/><br/>
If you are already a member, please <a href='/user/login?destination=node/9'>Login</a>.</p></center>";
  } 
  else {    
  					  
 print render($page['content']); 
      
    echo "<div class='usual'>";
    echo "<ul class='idTabs'>";
echo "<li><a href='#all-issued-docs'>Latest</a></li>";
echo "</ul>";
echo "<div id='all-issued-docs'>";
$view = views_get_view('network_newspaper_latest');
print $view->execute_display('default');
echo "</div>";
echo "<div id='all-received-docs'>";
echo "</div>";
echo "<div id='all-marked-nos'>";
echo "</div>"; 
echo "<p>Non Members can purchase a copy by clicking <a href='mailto:news@community-safety.net'>here</a></p>
      
<p>If you wish to become member and receive Network News free every month as part of the membership benefits, <a href='mailto:membership@community-safety.net'>please click here.</a></p>

<p>Have you got a news story of National interest?</p>
<p>We are also interested in promoting innovative approaches to reducing crime and disorder, so if you have such an initiative and wish to promote it nationally <a href='mailto:news@community-safety.net'>please click here</a></p>";


}
?>


</div>


     <!--  
       <div class="usual">
   
   
   <ul class="idTabs"> 
  <li><a href="#all-issued-docs">Latest</a></li> 
  <li><a href="#all-received-docs">Latest by date</a></li> 
</ul> 
<div id="all-issued-docs"> 
  <?php 
  //$view = views_get_view('latest_practice_in_action');
  //print $view->execute_display('default');
  ?>
  
  <span class="network-news-link"><a href="http://issuu.com/c8studio/docs/ncsn_august_11?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Fdark%2Flayout.xml&showFlipBtn=true">August 2011</a></span>
</div> 
<div id="all-received-docs">
<?php
//$view = views_get_view('practice_in_action_by_date');
//print $view->execute_display('default');
?>
<span class="network-news-link"><a href="http://issuu.com/c8studio/docs/ncsn_august_11?mode=embed&layout=http%3A%2F%2Fskin.issuu.com%2Fv%2Fdark%2Flayout.xml&showFlipBtn=true">August 2011</a></span>

</div>
<div id="all-marked-nos">

</div>
    
   </div>
-->


   
   

      <?php print $feed_icons; ?>

				</div><!-- end .entry-content -->

				<div class="entry-footer">
				
				<?php include('contact_info.tpl.php')?>

				</div><!-- end .entry-footer -->
				
			</div><!-- end .entry -->

		</div><!-- end #main -->

		<div id="sidebar">

			<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Sign up for our newsletter</h6>

					

				</div><!-- end .box-header -->


				<?php include('newsletter-form.tpl.php');?>


							</div><!-- end .flickr-feed -->
		
			
			
			<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Meet the Board</h6>

					
				</div><!-- end .box-header -->
				
				


				
				<?php include('meet-the-board.tpl.php');?>


			</div><!-- end .flickr-feed -->

			

			

		</div><!-- end #sidebar -->

		<div class="clear"></div>

	</div><!-- end .container -->

</div><!-- end #content -->

<?php include('footer.tpl.php');?>

</body>
</html>
