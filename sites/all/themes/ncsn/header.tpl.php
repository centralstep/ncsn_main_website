<div id="header-top">

	<div class="container">
	

		<!--<a href="#"><img src="/sites/all/themes/ncsn/images/icon-rss-large.png" alt="icon-rss-large" class="rss-icon" /></a>-->

		<p class="left"><span style="padding-left:30px; font-weight:500; font-size: 22px; font-family:Georgia;">National Community Safety Network - </span><span style="font-size:18px; font-family: Georgia;">Share our Vision for Safer Communities</span></p>

<a href="http://twitter.com/ncsn"><img src="/sites/all/themes/ncsn/images/twitter.png" style="margin-left:30px;"/></a>	<img src="/sites/all/themes/ncsn/images/linkedin.png"/>  <a href="http://community-safety.net/rss.xml"><img src="/sites/all/themes/ncsn/images/rss.png"></a>

		
		<?php global $user;?>
		<?php if ($user->uid == 0) {
		echo "<p class='right'><a href='/user/login' class='member-login'>Member Login</a></p>";}
		else {
		echo "<p class='right-logged-in'><a href='/user/' class='member-login'>Account</a> &nbsp; | &nbsp; <a href='/user/logout' class='member-login'>Logout</a>";
		}
		?>
	</p>
		
		
		<!--<a href="/user/login" class="member-login">Member Login</a>-->
 
		<!--Subscribe to <a href="#">RSS</a> | <a href="#">Email</a> | <strong>122 Subscribers</strong></p>-->

	</div><!-- end .container -->

</div><!-- end #header-top -->
<div id="nav">

	<div class="container">

		<ul>
			<li><a href="/">Home</a></li>
			<li><a href="/about">About</a></li>
			<li><a href="/forum">Forum</a></li>
			<li><a href="/membership">Membership</a></li>
			<li><a href="/information/network-newspaper">Network News</a></li>
			<li><a href="/contact">Contact us</a>
				
		</ul>
		
		<div id="search">
		
			<?php print render($page['search_block']);?>
			
		</div><!-- end #search -->



	</div><!-- end .container -->

</div><!-- end #nav -->

<div id="header" style="width:100%">

	<div class="container">

		<h1 id="logo">
			<a href="/">
				<img src="/sites/all/themes/ncsn/images/ncsn_logo.png" alt="National Community Safety Network" style="margin:-15px 0 0 -25px;">
			</a>
					</h1>
					
					<a href="/information" class="btn primary large" style="width:180px; height:14px; text-align:center; margin:50px 10px 0 80px;"><span>Information</span></a>
					<a href="/support" class="btn primary large" style="width:180px;height:14px; text-align:center;  margin: 0 10px 0 0;"><span>Support</span></a><br/><br/>
					<a href="/events" class="btn primary large" style="width:180px;height:14px; text-align:center;  margin:0 10px 0 80px;"><span>Conferences &amp; Seminars</span></a>

					<a href="/membership" class="btn primary large" style="width:180px; height:14px; text-align:center; "><span>Membership</span></a>
				
<!-- end #header-ads -->
		
			</div><!-- end .container -->

</div><!-- end #header -->

