<?php include('header.tpl.php');?>
<style type="text/css">

.form-item-status {display:none;}
.form-item-roles {display:none;}
.form-item-notify{display:none}
.form-item-field-payment-method-und {display:none;}

</style>
<div id="content" style="margin:20px 0 0 0;">
	<div class="container">
		<div id="main">
		<a id="main-content"></a>
    	<div class="entry single">
	    <div class="entry-header">
		<?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h2 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

					
					
					
					</div><!-- end .entry-header -->

		<?php print render($page['highlighted']); ?>
 <div class="breadcrumb"><a href="/">Home</a> &nbsp; > &nbsp; <a href="#"><?php print $title; ?></a> </div>



    			
					
				
				<div class="entry-content">
				
					       <?php print $messages; ?>
      <?php if ($tabs = render($tabs)): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>


      
  
  
  <div class="content">
  



<h4>As an FOC member - please ensure you select FOC membership from the drop down membership menu in the registration from</h4>

  
  <?php
  print drupal_render(drupal_get_form('user_register_form'));
?>



</div>

      <?php print $feed_icons; ?>

				</div><!-- end .entry-content -->

				<div class="entry-footer">
				
				<?php include('contact_info.tpl.php')?>

				</div><!-- end .entry-footer -->
				
			</div><!-- end .entry -->

		</div><!-- end #main -->

		<div id="sidebar">

			<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Sign up for our newsletter</h6>

					

				</div><!-- end .box-header -->


				<?php include('newsletter-form.tpl.php');?>


							</div><!-- end .flickr-feed -->
		
			
			
			<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Meet the Board</h6>

					
				</div><!-- end .box-header -->
				
				


				
				<?php include('meet-the-board.tpl.php');?>


			</div><!-- end .flickr-feed -->

			

		</div><!-- end #sidebar -->

		<div class="clear"></div>

	</div><!-- end .container -->

</div><!-- end #content -->

<?php include('footer.tpl.php');?>

</body>
</html>
