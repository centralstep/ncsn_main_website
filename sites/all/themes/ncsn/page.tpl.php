<?php include('header.tpl.php');?>

<div id="content" style="margin:20px 0 0 0;">
	<div class="container">

		<div id="main">
	<a id="main-content"></a>
      <div class="entry single">

				<div class="entry-header">

					 <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h2 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

					
					
					
					</div><!-- end .entry-header -->

		<?php print render($page['highlighted']); ?>
 <div class="breadcrumb"><a href="/">Home</a> &nbsp; > &nbsp; <a href="#"><?php print $title; ?></a> </div>



    			
					
				
				<div class="entry-content">
				
					            <?php if ($tabs = render($tabs)): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php print $messages;?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php //print render($content['field_page_image']); ?>
      <?php print $feed_icons; ?>

				</div><!-- end .entry-content -->

				<div class="entry-footer">
				
				<?php include('contact_info.tpl.php')?>

				</div><!-- end .entry-footer -->
				
			</div><!-- end .entry -->

		</div><!-- end #main -->

		<div id="sidebar">

			<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Sign up for our newsletter</h6>

					

				</div><!-- end .box-header -->


				<?php include('newsletter-form.tpl.php');?>


							</div><!-- end .flickr-feed -->
		
			
			
			<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Meet the Board</h6>

					
				</div><!-- end .box-header -->
				
				


				
				<?php include('meet-the-board.tpl.php');?>


			</div><!-- end .flickr-feed -->

			<div class="tags box">

				<div class="box-header">

					<h6>Safer Communities Tags</h6>

				</div><!-- end .box-header -->

				<ul>
					<li><a href="#">Theft</a></li>
					<li><a href="#">Assault</a></li>
					<li><a href="#">Snow</a></li>
					<li><a href="#">Weather</a></li>
					<li><a href="#">Storms</a></li>
					<li><a href="#">Rain</a></li>
					<li><a href="#">Road Safety</a></li>
					<li><a href="#">Safety in Schools</a></li>
					<li><a href="#">Safety @ Work</a></li>
					
				</ul>

			</div><!-- end .tags -->

			

		</div><!-- end #sidebar -->

		<div class="clear"></div>

	</div><!-- end .container -->

</div><!-- end #content -->

<?php include('footer.tpl.php');?>

</body>
</html>
