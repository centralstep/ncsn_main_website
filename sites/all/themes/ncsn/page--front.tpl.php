<?php include('header.tpl.php');?>


<div id="slider">

			<ul>
<?php 
// Get hold of all the current Front Page Visuals

//$sql = "SELECT * FROM {node} WHERE type='%s'";
$result = db_query("SELECT * FROM {node} WHERE type= :type AND status= :status",array(':type'=>'front_page_visual', ':status'=>1));
	foreach ($result as $record) {
	
	$nid = $record->nid;
	$imgID = db_query("SELECT field_fpv_image_fid FROM {field_data_field_fpv_image} WHERE entity_id= :d", array(':d'=>"$nid",))->fetchField();
	$img = db_query ("SELECT filename FROM {file_managed} WHERE fid= :s", array(':s'=>"$imgID",))->fetchField();
	$link = db_query("SELECT field_fpv_link_value FROM {field_revision_field_fpv_link} WHERE entity_id= :d", array(':d'=>"$nid",))->fetchField();
	$body = db_query("SELECT body_value FROM {field_data_body} WHERE entity_id= :d", array(':d'=>"$nid",))->fetchField();
	

	?>	
	<li>

<img src="/sites/default/files/<?php echo $img;?>" width="1010" height="300" />
<div class="slide-info">
<?php echo "<h2>" . $record->title . "</h2>"; ?>
<p class="meta"></p>
<?php echo "<p>" . $body . "</p>"; ?>
<a href="#" class="comments-count"></a>
<a href="/<?php echo $link;?>" class="slider-button">Read More...</a>
</div> <!-- End Slide Info -->
</li>

<?php } ?>
	
	</ul>
	<br/><br/>
		
		<div class="container">
<?php
     print render($page['header']); 
     ?>
     
     

	</div><!-- end .container -->
	
</div><!-- end #slider -->

<div id="content">
	<div class="container">
<?php print $messages;?>
		<div id="main">
			
			<div id="trips-viewer" class="box">

				<div class="box-header">

					<ul class="tabs-nav">
						<li><a href="#featured-trips">Welcome</a></li>
						<li><a href="#most-viewed-trips">Latest Forums</a></li>
					</ul>

					<a href="#" class="rss"><img src="/sites/all/themes/ncsn/images/icon-rss-small.png" alt="icon-rss" /></a>

			</div><!-- end .box-header -->

				<div id="featured-trips" class="tab-content">

					<div class="trips-container welcome-box" style="width:90%;">
						
						<h1 class="fancy-title">Welcome</h1>
												

								<p>NCSN is an organisation dedicated to supporting those involved in working and develop Safer Communities across the UK and throughout Europe. With a Board of Directors made up of experienced community safety practitioners and members from a wide range of organisations involved in crime reduction and community safety. NCSN is best placed to put you in touch with like minded individuals and provide the tools you need to support you in your role.</p>

<h1 class="fancy-title">Our mission</h1>

<span class="list-item">1) Give a National voice to those engaged in promoting Safer Communities through participation in consultation.</span><br/><br/>
<span class="list-item">2) Engage directly with government bodies on policy and practice in the field of community safety.</span><br/><br/>
<span class="list-item">3) Support the professional development of those engaged in delivering Safer Communities.</span><br/><br/>
<span class="list-item">4) Share information to promote joint working amongst relevant agencies across the UK and Europe.</span><br/>

<br/>

					
</div>
					

				</div><!-- end #featured-trips -->

			

				<div id="most-viewed-trips" class="tab-content">

					<div class="trips-container" style="width:95% !important; margin:0 auto !important;">

						<p>
	Dear member,<br />
	This your opportunity to influence the professional development of those operating in community safety, to set standards, and to offer recognition for the skills you all possess!</p>
<p>
	Thank you to everyone that attended the annual conference at Middlesex University last week and made it such a great event! One of the tasks, that came from the discussions by the Three Academics and the Audience, was to ask you all to consider what you think should form the basis of a job and person specification for a community safety manager. NCSN are involved in another European project,- <a href="http://www.urbisproject.eu/index.php/en/">URBIS.</a></p>
<p>
	Part of this is where thought is being given to the role of those managing urban security, or community safety as we know it, and what qualities and qualifications should be included when advertising such a post and thereby informing the development of the components for a suitable qualification.</p>
<p>
	So come on, reply and tell us what you think - but do it by 30th September please! We will collate the information and submit to the project lead, Professor Gordon Hughes, at Cardiff University. Alternatively you can contact Professor Hughes direct at: HughesGH@cardiff.ac.uk.</p>
<p>
	We will, of course, let you know what happens. please <a href="/events/september-conference-2013/feedback">click here to leave feedback</a></p>
<p>
	Best Wishes<br />
	June Armstrong<br />
	Vice Chair</p>

					</div><!-- end .trips-container -->

					<ul class="trips-nav">

						<li class="nav-button">
							<div class="content">
								<a href="#most-viewed-slide-one" class="title">
									<img src="/sites/all/themes/ncsn/images/sample-images/60x60.jpg" width="60" height="60" alt="" />
									<h6>Abroad Trip: Australian Rocks</h6>
								</a>
								<p class="meta">Published on <a href="#">March 08, 2010</a> by <a href="#">John Doe</a></p>
								<ul class="links">
									<li><a href="#">56 Comments</a> <span class="separator">|</span></li>
									<li><a href="#">7 Tweets</a> <span class="separator">|</span></li>
									<li><a href="#">Read More...</a></li>
								</ul>
							</div><!-- end .content -->
						</li><!-- end .nav-button -->

						<li class="nav-button">
							<div class="content">
								<a href="#most-viewed-slide-two" class="title">
									<img src="/sites/all/themes/ncsn/images/sample-images/60x60.jpg" width="60" height="60" alt="" />
									<h6>Museum: Native American Style Box</h6>
								</a>
								<p class="meta">Published on <a href="#">March 16, 2010</a> by <a href="#">John Doe</a></p>
								<ul class="links">
									<li><a href="#">64 Comments</a> <span class="separator">|</span></li>
									<li><a href="#">2 Tweets</a> <span class="separator">|</span></li>
									<li><a href="#">Read More...</a></li>
								</ul>
							</div><!-- end .content -->
						</li><!-- end .nav-button -->

						<li class="nav-button">
							<div class="content">
								<a href="#most-viewed-slide-three" class="title">
									<img src="/sites/all/themes/ncsn/images/sample-images/60x60.jpg" width="60" height="60" alt="" />
									<h6>Keep Off: Beach Attention Table</h6>
								</a>
								<p class="meta">Published on <a href="#">March 18, 2010</a> by <a href="#">John Doe</a></p>
								<ul class="links">
									<li><a href="#">34 Comments</a> <span class="separator">|</span></li>
									<li><a href="#">14 Tweets</a> <span class="separator">|</span></li>
									<li><a href="#">Read More...</a></li>
								</ul>
							</div><!-- end .content -->
						</li><!-- end .nav-button -->

						<li class="nav-button">
							<div class="content">
								<a href="#most-viewed-slide-four" class="title">
									<img src="/sites/all/themes/ncsn/images/sample-images/60x60.jpg" width="60" height="60" alt="" />
									<h6>Palmtree Wind Photo from Miami, Florida</h6>
								</a>
								<p class="meta">Published on <a href="#">March 21, 2010</a> by <a href="#">John Doe</a></p>
								<ul class="links">
									<li><a href="#">122 Comments</a> <span class="separator">|</span></li>
									<li><a href="#">9 Tweets</a> <span class="separator">|</span></li>
									<li><a href="#">Read More...</a></li>
								</ul>
							</div><!-- end .content -->
						</li><!-- end .nav-button -->

					</ul><!-- end .trips-nav -->

				</div><!-- end #most-viewed-trips -->

			</div><!-- end #trips-viewer -->

		</div>
		
		<div id="sidebar">
		<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Sign up for our newsletter</h6>

					

				</div><!-- end .box-header -->


				<?php include('newsletter-form.tpl.php');?>
<img src="/sites/all/themes/ncsn/images/Email-128-1.png" alt="image" width="64" height="64" style="position:absolute; margin: -240px 0 0 220px;" />

							</div><!-- end .flickr-feed -->


	<!--<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Upcoming Event</h6>

					

				</div>
 
<div style="padding:10px;">
	<h1 style="font-size:18px;">
		PCC Event 2011</h1>
		<h3 style="font-size:16px;">
		Tuesday 4th October 2011 - London</h3>
	<h3 style="font-size:16px;">
		Thursday 13th October 2011 - York</h3>
	<p style="font-size:14px;">
		Despite its recent defeat in the Lords, the Government is determined to introduce elected Police and Crime Commissioners(PCC).</p>
	<h3 style="font-size:16px;">
		<i>Download the event info <a href="/sites/default/files/pcc-event-october-2011.pdf" target="_blank">here</a></i></h3>
	<h3 style="font-size:16px;">
		<i>Download the event agenda <a href="/sites/default/files/pcc-event-october-2011-agenda.pdf" target="_blank">here</a></i></h3>
	<h3 style="font-size:16px;">
		<i>Download the registration form <a href="/sites/default/files/pcc-event-october-2011-reg-form.doc" target="_blank">here</a></i></h3>
</div>

				
			<?php //print render($page['second_sidebar']); ?>
		
	</div>-->
		



		
			
			<div id="recent-tabs" class="box">
			
				<div class="box-header">

					<ul class="nav">
						<li><a class="current" href="#recent-tabs-posts">Twitter</a></li>
						<!--<li><a href="#recent-tabs-comments">LinkedIN</a></li>-->
					</ul>

				</div><!-- end .box-header -->

				<div class="list-wrap">
<a class="twitter-timeline" href="https://twitter.com/NCSN" data-widget-id="390481278216052736">Tweets by @NCSN</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

					<!--<ul id="recent-tabs-posts">

						<li>
							<img src="/sites/all/themes/ncsn/images/twitter.png" alt="image" width="55" height="55" />
<a href="single-post.html" class="title">
																No Twitter Posts from NCSN yet.
							</a>
							<p class="meta">Tweeted on <a href="#">August 24th 2011</a> by <a href="http://twitter.com/ncsn">NCSN</a></p>
						</li>
-->
						
						
					</ul><!-- end #recent-tabs-comments -->

				</div><!-- end .list-wrap -->
				
			</div><!-- end #recent-tabs -->

			
		
			

		</div><!-- end #sidebar -->


<div class="clear"></div>
				
			<h1 style="font-size:35px;">Meet the Board</h1>


			<div class="entry" style="margin: 0 70px 0 0;">

				
				<div class="entry-header">

					<h2 class="title"><a href="single-post.html">Robert Greaves</a></h2>

					<p class="meta">Chair</p>
					<a href="/about/board/robert-greaves" class="small blue button">View Profile</a>

				</div><!-- end .entry-header -->

				<div class="entry-content">

					<a href="/about/board/robert-greaves"><img src="/sites/all/themes/ncsn/images/robertgreaves.jpg" width="137" height="143" alt="" class="entry-image" /></a>

					<p>Robert has over 20 years experience in managing Community Safety and over 30 years experience of the public sector, having retired as Head of Community Safety at Wandsworth in 2010. 
Robert is Secretary of Wandsworth Community Safety Trust, has a number of other charitable interests and is currently a Visiting Fellow at Portsmouth University Institute for Criminal Justice Studies. 
Robert has recently completed a short term contract as Census Area Manager for Wandsworth West. Robert has been an NCSN Board Director since 2003 and is currently Chair of NCSN.
 </p>
 <br/><br/>
					<hr />

					<ul class="entry-links">
						<li><a href="mailto:directors@community-safety.net?subject=Website Enquiry">E-mail Robert</a> <span class="separator">|</span></li>
						
						</ul>

				</div><!-- end .entry-content -->

				

			</div><!-- end .entry -->
			
			

			
			<div class="entry" style="margin: 0 -40px 10px 0;">

				<div class="entry-header">

					<h2 class="title"><a href="single-post.html">June Armstrong</a></h2>

					<p class="meta">Vice Chair</p>

					<a href="/about/board/june-armstrong" class="small blue button">View Profile</a>

				</div><!-- end .entry-header -->

				<div class="entry-content">

					<a href="single-post.html"><img src="/sites/all/themes/ncsn/images/juneamrstrong.jpg" width="137" height="167" alt="" class="entry-image" /></a>

					<p>June is currently vice chair of the National Community Safety Network with responsibility for learning and development, events and HR. Formerly the projects manager for the crime reduction unit at the University of Central Lancashire (Uclan) and chair of the North West Truckwatch scheme, she has over 25 years experience in crime prevention, community safety, counter terrorism and risk management with the police, the Home Office and the private sector. She has developed and delivered crime prevention and community safety training at local, national and international levels. June has extensive experience of working with SMEs and is a member of the Chartered Institute of Personnel and Development, institute of Risk Managers and the Institute of Conflict Management.</p>
					<p>&nbsp;</p>

					<hr />

					<ul class="entry-links">
						<li><a href="mailto:directors@community-safety.net?subject=Website Enquiry">E-mail June</a> <span class="separator">|</span></li>
						
						</ul>

			</div><!-- end .entry-content -->


			</div><!-- end .entry -->
			
			
			<div class="entry" style="margin-right:66px;">

				<div class="entry-header">

					<h2 class="title"><a href="single-post.html">Michael McCrory</a></h2>

					<p class="meta">Vice Chair</p>

					<a href="/about/board/michael-mccrory" class="small blue button">View Profile</a>

				</div><!-- end .entry-header -->

				<div class="entry-content">

					<a href="/about/board/michael-mccrory"><img src="/sites/all/themes/ncsn/images/michaelmcrory.jpg" width="137" height="143" alt="" class="entry-image" /></a>

					<p>Michael is currently the Towns and Community Regeneration Manager for Magherafelt District Council.  He is responsible for developing and implementing strategies and action plans for Community Safety. He also manages the District Policing Partnership, which is a statutory body that monitors the performance of the police in delivering against the policing plan. The final area of work that Michael is responsible for is Community Planning and Regeneration.
Michael is part of the Community Safety Managers Forum in Northern Ireland and represents the region on the NCSN board. Previously Michael worked in the field of Community Development
 </p>
<br/>
					<hr />

					<ul class="entry-links">
						<li><a href="mailto:directors@community-safety.net?subject=Website Enquiry">E-mail Michael</a> <span class="separator">|</span></li>
						
						</ul>

				</div><!-- end .entry-content -->

			</div><!-- end .entry -->

<div class="entry" style="margin: 0 0px 0 0;">

				
				<div class="entry-header">

					<h2 class="title"><a href="single-post.html">John Spitzer</a></h2>

					<p class="meta">Director</p>

					<a href="/about/board/john-spitzer" class="small blue button">View Profile</a>

				</div><!-- end .entry-header -->

				<div class="entry-content">

						<a href="single-post.html"><img src="/sites/all/themes/ncsn/images/johnspitzer.jpg" width="137" height="205" alt="" class="entry-image" /></a>

				
					<p>A skilled community-focused professional with over 20 years of experience across law enforcement, criminal justice, probation, prison service, community engagement, project and programme management. With a blend of experience in Local Government, Third Sector, law enforcement, military, and private companies, John has been able to develop skills in conceptual, practical, and profitable leadership and management arenas through his community service work. He developed and led the John Peel Centre for Creative Arts CIC, Chaired Domestic Abuse Forums, organised the largest DV conference in the East of England, and has been selected and elected as Director of the National Community Safety Network Charity. 
John is currently the Senior Operations Manager  (Deputy Operational Director) for the British Red Cross in London with responsibilities for strategic planning, internal operations, change, project and KPI management. London Area Gold Commander for event and crises response.
Previously the Community Safety Manager at Mid Suffolk District Council with responsibility for the community safety team, driving strategic planning and implementation of community safety related initiatives, covering crime reduction, ASB services, licensing, planning, domestic abuse service, community development, police/voluntary/statutory relationships, educational initiatives, and the health link. 
He also served as liaison to elected representatives on matters involving community safety, prepares and presents scrutiny, policy panel reports, project evaluations and also drives operational delivery on diverse projects. </p>

					
					
								
					
					<hr />

					<ul class="entry-links">
						<li><a href="mailto:directors@community-safety.net?subject=Website Enquiry"></li>E-mail John</a> <span class="separator">|</span></li>
					
						</ul>

				</div><!-- end .entry-content -->

				

			</div><!-- end .entry -->
			
			
			
			<div class="entry" style="margin-right:70px;">

				<div class="entry-header">

					<h2 class="title"><a href="single-post.html">Mark McCormack</a></h2>

					<p class="meta">Director</p>

					<a href="/about/board/mark-mccormak" class="small blue button">View Profile</a>

				</div><!-- end .entry-header -->

				<div class="entry-content">

					<a href="single-post.html"><img src="/sites/all/themes/ncsn/images/markmccormack.jpg" width="137" height="159" alt="" class="entry-image" /></a>

					<p>I am the current General Manager of Cambridge University Students Union and have worked within student-led, membership organisations for the past five years.
In my previous post at the National Union of Students, I managed a Home Office project to reduce crime against students in England and Wales. Within the project I managed a national campaign to reduce crime: working with student volunteers and cross-sector partners, working closely with the Home Office and national policing associations, forming the first national steering committee for student-crime, developing policy and research in the area, and coordinating activities looking at behaviour change and social networking in crime reduction.
 </p>
 <p>&nbsp;</p><p>&nbsp;</p>

					<ul class="entry-links">
						<li><a href="mailto:directors@community-safety.net?subject=Website Enquiry">E-mail Mark</a> <span class="separator">|</span></li>
						
						</ul>

				</div><!-- end .entry-content -->

			</div><!-- end .entry -->
			
				<div class="entry" style="margin: 0 -40px 0 0;">

				<div class="entry-header">

					<h2 class="title"><a href="single-post.html">Andy Pownall</a></h2>

					<p class="meta">Director</p>

					<a href="/about/board/andy-pownall" class="small blue button">View Profile</a>

				</div><!-- end .entry-header -->

				<div class="entry-content">

					<a href="single-post.html"><img src="/sites/all/themes/ncsn/images/andypownall_pic.jpg" width="137" height="159" alt="" class="entry-image" /></a>

<p>
		I presently work as Community Safety Manager for Greater Manchester Fire and Rescue Service which incorporates all crime and disorder implications which affect the service. Prior to this I have worked at the Home Office for six years managing National Violence programmes and working with CSPs in the North West and Yorkshire and Humber.
		I have also managed a multi agency Community Safety team in Halton and have extensive knowledge and experience in the field of Community Safety spanning over 12 years.		</p>
				<br/><br/><br/><br/><br/>
					<hr />

					<ul class="entry-links">
						<li><a href="mailto:directors@community-safety.net?subject=Website Enquiry">E-mail Andy</a> <span class="separator">|</span></li>
					
						</ul>

				</div><!-- end .entry-content -->

			</div><!-- end .entry -->



			





			
		
						
						

		</div><!-- end #main -->

		
		<div class="clear"></div>

	</div><!-- end .container -->

</div><!-- end #content -->

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php include('footer.tpl.php');?>


</body>
</html>

