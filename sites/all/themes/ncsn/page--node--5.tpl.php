
<!-- Template for the information page -->

<?php include('header.tpl.php');?>


<div id="content" style="margin:20px 0 0 0;">
	<div class="container">

		<div id="main" style="width:100%;">
	<a id="main-content"></a>
      <div class="entry single">

				<div class="entry-header">

					 <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h2 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

					
					
					
					</div><!-- end .entry-header -->

		<?php print render($page['highlighted']); ?>
      <div class="breadcrumb"><a href="/">Home</a> &nbsp; > &nbsp; <a href="#">Information</a> </div>


    			
					
				
				<div class="entry-content">
				
					       <?php print $messages; ?>
      <?php if ($tabs = render($tabs)): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
      
      
      
      
      <div class="mainContent33">
      <h2><a href="/information/practice-in-action">Practice in Action</a></h2>
      <p class="description">Effective Practice, What Works and What Doesn't.<br/>
'Practice in Action' will include examples of Community Safety Initiatives &amp; Projects from across the UK and Europe. 
</p>
      <span class="readMore"><a href="/information/practice-in-action">read more...</a></span>
      
      </div>
      <div class="mainContent33">
      <h2><a href="/information/network-newspaper">Network Newspaper</a></h2>
      <p class="description">The NCSN monthly newspaper, Network News brings you the latest national and local community safety news items from around the UK and Europe. 

</p>
 <span class="readMore"><a href="/information/network-newspaper">read more...</a></span>
      


      </div>
      <div class="mainContent33">
      <h2><a href="/information/news">News</a></h2>
      <p class="description">People from across the Public, Community, Voluntary and Business sectors are working to reduce crime & disorder and make communities safer. Therefore this section captures all the miscellaneous Community Safety News from across the UK.</p>
      <span class="readMore"><a href="/information/news">read more...</a></span>
      </div>
      
      <div class="mainContent33">
      <h2><a href="/forum">The Network</a></h2>
      <p class="description">The 'Network' is an on line forum where partnership colleagues working to reduce crime and disorder can network on line.</p>
      <span class="readMore"><a href="/forum">read more...</a></span>
      </div>
      
      <div class="mainContent33">
      <h2><a href="/information/sixty-second-briefings">Sixty Second Briefings</a></h2>
      <p class="description">NCSN produce regular '60 second policy briefings' on key community safety topics for NCSN members. 

</p>
<span class="readMore"><a href="/information/sixty-second-briefings">read more...</a></span>


      </div>
      <div class="mainContent33">
      <h2><a href="/information/consultations-and-surveys">Consultation &amp; Surveys</a></h2>
      <p class="description">Periodically NCSN conduct consultation and surveys to establish members thoughts and responses to legislative changes, Government Policy and changes that effect working practices across CSPs, agencies and  Public, Voluntary/Community and Private sectors.</p>
      <span class="readMore"><a href="/information/consultations-and-surveys">read more...</a></span>
      </div>
       <div class="mainContent33">
      <h2><a href="/information/violent-crime">Violent Crime</a></h2>
      <p class="description">NCSN are part of a European Research Project on Urban Street Violence as part of a programme funded by the European Commission and led by the European Forum for Urban Security (Efus). </p>
<span class="readMore"><a href="/information/violent-crime">read more...</a></span>
      </div>
       <div class="mainContent33">
      <h2><a href="/information/europe">Europe</a></h2>
      <p class="description">	NCSN is working with four other countries in a project to devise a database of recommendations on how to tackle Urban Street Violence. 	</p>
<span class="readMore"><a href="/information/europe">read more...</a></span>


      </div>
      
      
      
      
      
      

				</div><!-- end .entry-content -->

				<div class="entry-footer">

					
				<?php include('contact_info.tpl.php')?>		
				
						</div><!-- end .entry-footer -->
				
			</div><!-- end .entry -->

		</div><!-- end #main -->

				<div class="clear"></div>

	</div><!-- end .container -->

</div><!-- end #content -->
<?php include('footer.tpl.php');?>
</body>
</html>
