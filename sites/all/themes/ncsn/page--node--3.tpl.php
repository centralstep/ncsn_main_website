
<!-- Template for the information page -->

<?php include('header.tpl.php');?>


<div id="content" style="margin:20px 0 0 0;">
	<div class="container">

		<div id="main" style="width:100%;">
	<a id="main-content"></a>
      <div class="entry single">

				<div class="entry-header">

					 <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h2 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

					
					
					
					</div><!-- end .entry-header -->

		<?php print render($page['highlighted']); ?>
      <div class="breadcrumb"><a href="/">Home</a> &nbsp; > &nbsp; <a href="#">Membership</a> </div>


    			
					
				
				<div class="entry-content">
				
					       <?php print $messages; ?>
      <?php if ($tabs = render($tabs)): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
      
  
      
      

				</div><!-- end .entry-content -->

				<div class="entry-footer">

					
				<?php include('contact_info.tpl.php')?>		
				
						</div><!-- end .entry-footer -->
				
			</div><!-- end .entry -->

		</div><!-- end #main -->

				<div class="clear"></div>

	</div><!-- end .container -->

</div><!-- end #content -->
<?php include('footer.tpl.php');?>
</body>
</html>

<script type="text/javascript">
	
	$('table.pricing-1').each( function()
	{
		index = $(this).find('table').index($(this).find('table.current'));
		
		$(this).data('default-col', index);
	});
	
	$('table.pricing-1').hover( function()
	{

	}, function()
	{
		index = $(this).data('default-col');
		
		$(this).find('table').removeClass('current').eq(index).addClass('current');
	});
	
	$('table.pricing-1 td > table').hover( function()
	{
		$(this).parents('table.pricing-1').find('table').removeClass('current');
		$(this).addClass('current');
	}, function()
	{
		$(this).removeClass('current');
	});

	
	$('td').each( function()
	{
		if (typeof $(this).attr('custom-description') != 'undefined' && $(this).attr('custom-description') != '')
		{
			desc = $(this).attr('custom-description');
			$(this).append($('<span />').hide().addClass('tooltip').html('<span>' + desc + '</span>'));
		}
	});
	
	$('td').tooltip
	({
		tooltip: '.tooltip',
		x: 20,
		y: 25,
		offset: false
	});
	
	
	</script>