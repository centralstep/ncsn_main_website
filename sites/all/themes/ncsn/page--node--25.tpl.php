<?php include('header.tpl.php');?>

<div id="content" style="margin:20px 0 0 0;">
	<div class="container">

		<div id="main">
	<a id="main-content"></a>
      <div class="entry single">

				<div class="entry-header">

					 <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h2 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

					
					
					
					</div><!-- end .entry-header -->

		<?php print render($page['highlighted']); ?>
 <div class="breadcrumb"><a href="/">Home</a> &nbsp; > &nbsp; <a href="/about">About</a> &nbsp; > &nbsp; <a href="#">Board</a> &nbsp; > &nbsp; <a href="#"><?php print $title; ?></a> </div>



    			
					
				
				<div class="entry-content">
				
					       <?php print $messages; ?>
      <?php if ($tabs = render($tabs)): ?>
        <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>

				</div><!-- end .entry-content -->

				<div class="entry-footer">
				
				<?php include('contact_info.tpl.php')?>

				</div><!-- end .entry-footer -->
				
			</div><!-- end .entry -->

		</div><!-- end #main -->

		<div id="sidebar">
		
		<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Meet the Board</h6>

					

				</div><!-- end .box-header -->
				
				


				<?php include('meet-the-board.tpl.php');?>

			</div><!-- end .flickr-feed -->


			<div class="flickr-feed box">

				<div class="box-header">
					
					<h6 class="align-left">Sign up for our newsletter</h6>

					

				</div><!-- end .box-header -->


				<?php include('newsletter-form.tpl.php');?>


							</div><!-- end .flickr-feed -->
		
			
			<div id="recent-tabs" class="box">
			
				<div class="box-header">

					<ul class="nav">
						<li><a class="current" href="#recent-tabs-posts">Upcoming Events</a></li>
						<li><a href="#recent-tabs-comments">Past Events</a></li>
					</ul>

				</div><!-- end .box-header -->

				<div class="list-wrap">

					<ul id="recent-tabs-posts">

						<li>
							<a href="single-post.html" class="title">
								<img src="/sites/all/themes/ncsn/images/sample-images/40x40.jpg" width="40" height="40" alt="" />
								June Conference
							</a>
							<p class="meta">Published on <a href="#">March 29, 2010</a> by <a href="#">John Doe</a></p>
						</li>

						<li>
							<a href="single-post.html" class="title">
								<img src="/sites/all/themes/ncsn/images/sample-images/40x40.jpg" width="40" height="40" alt="" />
								June Conference
							</a>
							<p class="meta">Published on <a href="#">March 29, 2010</a> by <a href="#">John Doe</a></p>
						</li>

						<li>
							<a href="single-post.html" class="title">
								<img src="/sites/all/themes/ncsn/images/sample-images/40x40.jpg" width="40" height="40" alt="" />
								August Conference
							</a>
							<p class="meta">Published on <a href="#">March 29, 2010</a> by <a href="#">John Doe</a></p>
						</li>

						<li>
							<a href="single-post.html" class="title">
								<img src="/sites/all/themes/ncsn/images/sample-images/40x40.jpg" width="40" height="40" alt="" />
								National Community Safety Event
							</a>
							<p class="meta">Published on <a href="#">March 29, 2010</a> by <a href="#">John Doe</a></p>
						</li>

						<li>
							<a href="single-post.html" class="title">
								<img src="/sites/all/themes/ncsn/images/sample-images/40x40.jpg" width="40" height="40" alt="" />
								September Seminar
							</a>
							<p class="meta">Published on <a href="#">March 29, 2010</a> by <a href="#">John Doe</a></p>
						</li>

					</ul><!-- end #recent-tabs-posts-->

					<ul id="recent-tabs-comments" class="hide">
					
						<li>
							<a href="single-post.html" class="title">
								<img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=40" width="40" height="40" alt="" />
								Lorem ipsum dolor sit amet...
							</a>
							<p class="meta">Commented on <a href="#">March 29, 2010</a> by <a href="#">John Doe</a></p>
						</li>
						
						<li>
							<a href="single-post.html" class="title">
								<img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=40" width="40" height="40" alt="" />
								Lorem Ipsum is simply dummy text...
							</a>
							<p class="meta">Commented on <a href="#">March 17, 2010</a> by <a href="#">John Doe</a></p>
						</li>

						<li>
							<a href="single-post.html" class="title">
								<img src="http://1.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=40" width="40" height="40" alt="" />
								Lorem ipsum dolor sit amet...
							</a>
							<p class="meta">Commented on <a href="#">March 3, 2010</a> by <a href="#">John Doe</a></p>
						</li>
						
					</ul><!-- end #recent-tabs-comments -->

				</div><!-- end .list-wrap -->
				
			</div><!-- end #recent-tabs -->

			
			

			

		</div><!-- end #sidebar -->

		<div class="clear"></div>

	</div><!-- end .container -->

</div><!-- end #content -->

<?php include('footer.tpl.php');?>

</body>
</html>
